package com.example.androiddevelopertest.domain

import androidx.paging.PagingData
import com.example.androiddevelopertest.domain.entities.Job
import kotlinx.coroutines.flow.Flow

interface UseCase {

    fun getJob(
        description: String? = null,
        location: String? = null,
        fullTime: Boolean? = null,
    ) : Flow<PagingData<Job>>

    fun getDetailJob(id: String) : Flow<Resource<Job>>

}