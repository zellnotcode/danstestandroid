package com.example.androiddevelopertest.domain.entities

data class Job(
    val companyLogo: String? = null,
    val howToApply: String? = null,
    val createdAt: String? = null,
    val description: String? = null,
    val company: String? = null,
    val companyUrl: String? = null,
    val location: String? = null,
    val id: String? = null,
    val type: String? = null,
    val title: String? = null,
    val url: String? = null
)