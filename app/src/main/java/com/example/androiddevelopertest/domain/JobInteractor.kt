package com.example.androiddevelopertest.domain

import androidx.paging.PagingData
import com.example.androiddevelopertest.domain.entities.Job
import kotlinx.coroutines.flow.Flow

class JobInteractor constructor(private val repository: Repository) : UseCase {
    override fun getJob(
        description: String?,
        location: String?,
        fullTime: Boolean?
    ): Flow<PagingData<Job>> = repository.getJob(description, location, fullTime)

    override fun getDetailJob(id: String): Flow<Resource<Job>> = repository.getDetailJob(id)
}