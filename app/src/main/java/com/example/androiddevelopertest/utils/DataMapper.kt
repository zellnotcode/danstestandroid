package com.example.androiddevelopertest.utils

import com.example.androiddevelopertest.data.response.JobListResponseItem
import com.example.androiddevelopertest.domain.entities.Job

object DataMapper {
    fun jobResponseToDomain(item: JobListResponseItem?): Job =
        if (item != null) {
            Job(
                companyLogo = item.companyLogo,
                howToApply = item.howToApply,
                createdAt = item.createdAt,
                description = item.description,
                company = item.company,
                companyUrl = item.companyUrl,
                location = item.location,
                id = item.id,
                type = item.type,
                title = item.title,
                url = item.url
            )
        } else {
             Job()
        }
}