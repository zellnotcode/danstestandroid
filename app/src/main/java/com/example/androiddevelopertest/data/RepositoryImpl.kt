package com.example.androiddevelopertest.data

import androidx.paging.PagingData
import androidx.paging.filter
import androidx.paging.map
import com.example.androiddevelopertest.domain.Repository
import com.example.androiddevelopertest.domain.Resource
import com.example.androiddevelopertest.domain.entities.Job
import com.example.androiddevelopertest.utils.DataMapper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

class RepositoryImpl constructor(private val networkDataSource: NetworkDataSource) : Repository {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override fun getJob(
        description: String?,
        location: String?,
        fullTime: Boolean?
    ): Flow<PagingData<Job>> {
        val pagingData = networkDataSource.getJob(description, location, fullTime)

        return pagingData.map { pagingDataResponse ->
            pagingDataResponse.filter {it != null}.map {
                DataMapper.jobResponseToDomain(it)
            }
        }
    }

    override fun getDetailJob(id: String): Flow<Resource<Job>> {
        return flow<Resource<Job>> {
            emit(Resource.Loading())
            networkDataSource.getDetailJob(id).collect { result ->
                when (result) {
                    is ApiResponse.Success -> {
                        val userResponse = result.data
                        val data = DataMapper.jobResponseToDomain(userResponse)
                        emit(Resource.Success(data))
                    }

                    is ApiResponse.Empty -> {
                        emit(Resource.Error("Data Empty"))
                    }

                    is ApiResponse.Error -> {
                        emit(Resource.Error(result.errorMessage))
                    }
                }
            }
        }.flowOn(ioDispatcher)
    }
}