package com.example.androiddevelopertest.data

import com.example.androiddevelopertest.data.response.JobListResponseItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("positions.json")
    suspend fun getJobList(
        @Query("description") description: String?,
        @Query("location") location: String?,
        @Query("full_time") fullTime: Boolean?,
        @Query("page") page: Int?
    ): List<JobListResponseItem>

    @GET("positions/{id}")
    suspend fun getDetailJob(
        @Path("id") id: String
    ): Response<JobListResponseItem>
}