package com.example.androiddevelopertest.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.androiddevelopertest.data.pagingsource.JobPagingSource
import com.example.androiddevelopertest.data.response.JobListResponseItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class NetworkDataSource constructor(private val apiService: ApiService) {

    fun getJob(
        description: String? = null,
        location: String? = null,
        fullTime: Boolean? = null
    ): Flow<PagingData<JobListResponseItem>> {
        return Pager(
            config = PagingConfig(
                pageSize = 5,
                enablePlaceholders = true
            ),
            pagingSourceFactory = {
                JobPagingSource(apiService, description, location, fullTime)
            }
        ).flow
    }

    fun getDetailJob(id: String): Flow<ApiResponse<JobListResponseItem>> = flow {
        try {
            val response = apiService.getDetailJob(id)
            if (response.isSuccessful) {
                val user = response.body()
                if (user != null) {
                    emit(ApiResponse.Success(user))
                } else {
                    emit(ApiResponse.Empty)
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
        }
    }

}