package com.example.androiddevelopertest.data.response

import com.google.gson.annotations.SerializedName

data class JobListResponse(

	@field:SerializedName("JobListResponse")
	val jobListResponse: List<JobListResponseItem?>? = null
)