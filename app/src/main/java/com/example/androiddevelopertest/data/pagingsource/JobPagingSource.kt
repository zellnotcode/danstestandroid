package com.example.androiddevelopertest.data.pagingsource

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.androiddevelopertest.data.ApiService
import com.example.androiddevelopertest.data.response.JobListResponseItem

class JobPagingSource constructor(
    private val apiService: ApiService,
    private val description: String? = null,
    private val location: String? = null,
    private val fullTime: Boolean? = null,
) : PagingSource<Int, JobListResponseItem>() {
    override fun getRefreshKey(state: PagingState<Int, JobListResponseItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, JobListResponseItem> {
        return try {
            val page = params.key ?: 1
            val responseData = apiService.getJobList(description, location, fullTime, page)
            val prevKey = if (page == 1) null else page - 1
            val nextKey = if (responseData.isEmpty()) null else page + 1
            LoadResult.Page(
                data = responseData,
                prevKey = prevKey,
                nextKey = nextKey
            )

        } catch (e: Exception) {
            Log.e("PagingSource", e.toString())
            LoadResult.Error(e)
        }
    }

}