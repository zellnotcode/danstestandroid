package com.example.androiddevelopertest.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androiddevelopertest.R
import com.example.androiddevelopertest.databinding.FragmentHomeBinding
import com.example.androiddevelopertest.presentation.adapter.JobListAdapter
import com.example.androiddevelopertest.presentation.adapter.LoadingStateAdapter
import com.example.androiddevelopertest.presentation.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel: HomeViewModel by viewModel()
    private var isOpenMenu = false
    private var isFullTime = false
    private val jobListAdapter: JobListAdapter by lazy {
        JobListAdapter { id ->
           val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(id)
            findNavController().navigate(action)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.menuIcon.setOnClickListener { handleMenu() }

        with(binding) {
            rvJob.layoutManager = LinearLayoutManager(requireContext())
            rvJob.adapter = jobListAdapter.withLoadStateFooter(
                LoadingStateAdapter()
            )
        }

        binding.switchFulltime.setOnCheckedChangeListener { _, isChecked ->
            isFullTime = isChecked
        }

        jobListAdapter.addLoadStateListener { loadState ->
            when (loadState.refresh) {
                is LoadState.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                is LoadState.Error -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), "Failed Get User Data", Toast.LENGTH_SHORT)
                        .show()
                }
                is LoadState.NotLoading -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        }

        binding.btnApply.setOnClickListener {
            applyFilter()
        }

        setupJobList()
        setSearchUser()
    }

    private fun applyFilter() {
        val location = binding.etLocation.text
        if (!location.isNullOrEmpty()) {
            homeViewModel.getJob(
                location = location.toString(),
                fullTime = isFullTime
            ).observe(viewLifecycleOwner) {
                jobListAdapter.submitData(lifecycle, it)
            }
        } else {
            homeViewModel.getJob(
                fullTime = isFullTime
            ).observe(viewLifecycleOwner) {
                jobListAdapter.submitData(lifecycle, it)
            }
        }
    }

    private fun setupJobList() {
        homeViewModel.getJob().observe(viewLifecycleOwner) { pagingData ->
            jobListAdapter.submitData(lifecycle, pagingData)
        }
    }

    private fun handleMenu() {
        if (isOpenMenu) {
            isOpenMenu = false
            setImageMenu(false)
            binding.dropdownContainer.visibility = View.GONE
        } else {
            isOpenMenu = true
            setImageMenu(true)
            binding.dropdownContainer.visibility = View.VISIBLE
        }
    }

    private fun setSearchUser() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty()) {
                    setupJobList()
                }
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!query.isNullOrEmpty()) {
                    homeViewModel.getJob(description = query).observe(viewLifecycleOwner) { listUser ->
                        jobListAdapter.submitData(lifecycle, listUser)
                    }
                }
                return false
            }
        })
    }

    private fun setImageMenu(isOpen: Boolean) {
        if(isOpen) {
            binding.menuIcon.setImageDrawable(ContextCompat.getDrawable(binding.menuIcon.context, R.drawable.ic_expand_less))
        } else
            binding.menuIcon.setImageDrawable(ContextCompat.getDrawable(binding.menuIcon.context, R.drawable.ic_more_expand))
    }

    override fun onStop() {
        super.onStop()
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}