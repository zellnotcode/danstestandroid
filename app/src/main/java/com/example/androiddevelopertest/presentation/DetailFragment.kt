package com.example.androiddevelopertest.presentation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.androiddevelopertest.databinding.FragmentDetailBinding
import com.example.androiddevelopertest.domain.Resource
import com.example.androiddevelopertest.presentation.viewmodel.DetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args: DetailFragmentArgs by navArgs()
    private val detailViewModel: DetailViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }

        setDetail()
    }

    private fun setDetail() {
        detailViewModel.fetchJobDetail(args.id)

        detailViewModel.detailLiveData.observe(viewLifecycleOwner) { resource ->
            when(resource) {
                is Resource.Loading -> {}

                is Resource.Success -> {
                    if (resource.data != null) {
                        with(binding) {
                            tvJobCompany.text = resource.data.company
                            tvJobLocation.text = resource.data.location
                            tvJobTitle.text = resource.data.title
                            Glide.with(requireContext())
                                .load(resource.data.companyLogo)
                                .centerCrop()
                                .into(ivJob)

                            linkTextView.setOnClickListener {
                                val linkText = resource.data.companyUrl.toString()
                                if (Patterns.WEB_URL.matcher(linkText).matches()) {
                                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(linkText))
                                    startActivity(intent)
                                }
                            }

                            tvJobFulltime.text = if (resource.data.type == "Full Time") {
                                "Yes"
                            } else {
                                "No"
                            }

                            val htmlContent = resource.data.description.toString()
                            webView.loadDataWithBaseURL(null, htmlContent, "text/html", "UTF-8", null)
                        }
                    }
                }

                is Resource.Error -> {
                    Log.e("DetailFragment", resource.message.toString())
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}