package com.example.androiddevelopertest.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.androiddevelopertest.domain.UseCase
import com.example.androiddevelopertest.domain.entities.Job

class HomeViewModel constructor(private val useCase: UseCase) : ViewModel() {

    fun getJob(
        description: String? = null,
        location: String? = null,
        fullTime: Boolean? = null,
    ): LiveData<PagingData<Job>> =
        useCase.getJob(description, location, fullTime).asLiveData().cachedIn(viewModelScope)

}