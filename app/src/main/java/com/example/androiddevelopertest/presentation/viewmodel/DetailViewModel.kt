package com.example.androiddevelopertest.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androiddevelopertest.domain.Resource
import com.example.androiddevelopertest.domain.UseCase
import com.example.androiddevelopertest.domain.entities.Job
import kotlinx.coroutines.launch

class DetailViewModel constructor(private val useCase: UseCase) : ViewModel() {

    private val _detailLiveData: MutableLiveData<Resource<Job>> = MutableLiveData()
    val detailLiveData: LiveData<Resource<Job>> get() = _detailLiveData

    fun fetchJobDetail(id: String) {
        viewModelScope.launch {
            useCase.getDetailJob(id)
                .collect { resource ->
                    _detailLiveData.value = resource
                }
        }
    }
}