package com.example.androiddevelopertest.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.androiddevelopertest.databinding.JobItemBinding
import com.example.androiddevelopertest.domain.entities.Job

class JobListAdapter(private val onClickItem: (id: String) -> Unit) :
    PagingDataAdapter<Job, JobListAdapter.ViewHolder>(DIFF_CALLBACK) {

    class ViewHolder(var binding: JobItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(job: Job) {
            binding.apply {
                tvJobTitle.text = job.title
                tvJobCompany.text = job.company
                tvJobLocation.text = job.location
                Glide.with(itemView.context)
                    .load(job.companyLogo)
                    .centerCrop()
                    .into(ivJob)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { job ->
            holder.bind(job)
            holder.binding.root.setOnClickListener {
                onClickItem(job.id.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = JobItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false)
        return ViewHolder(binding)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Job>() {
            override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}