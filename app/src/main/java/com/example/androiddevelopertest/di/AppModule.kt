package com.example.androiddevelopertest.di

import com.example.androiddevelopertest.BuildConfig
import com.example.androiddevelopertest.data.ApiService
import com.example.androiddevelopertest.data.NetworkDataSource
import com.example.androiddevelopertest.data.RepositoryImpl
import com.example.androiddevelopertest.domain.JobInteractor
import com.example.androiddevelopertest.domain.Repository
import com.example.androiddevelopertest.domain.UseCase
import com.example.androiddevelopertest.presentation.viewmodel.DetailViewModel
import com.example.androiddevelopertest.presentation.viewmodel.HomeViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

var appModule = module {

    single {
        val loggingInterceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        } else {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
        }

        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()
            .create(ApiService::class.java)
    }

    single {
        NetworkDataSource(get())
    }

    single<Repository> {
        RepositoryImpl(get())
    }

    single<UseCase> {
        JobInteractor(get())
    }

    viewModel{ HomeViewModel(get()) }
    viewModel { DetailViewModel(get()) }
}