package com.example.androiddevelopertest

import android.app.Application
import com.example.androiddevelopertest.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.context.unloadKoinModules

class JobApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@JobApp)
            loadKoinModules(appModule)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        unloadKoinModules(appModule)
    }
}